from django.db import models
from django.utils import timezone

# Create your models here.
class Forum(models.Model):
    user_id = models.CharField(max_length=200)
    # title = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    body = models.CharField(max_length=200)
    # created_date = models.DateTimeField(default=timezone.now)

    
class Comment(models.Model):
    user_id = models.CharField(max_length=200)
    post = models.ForeignKey(Forum, related_name='comments')
    name = models.CharField(max_length=200)
    body = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    class Meta:
        ordering = ['created_date']