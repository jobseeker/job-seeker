from django.conf.urls import url
from .views import index,addPost,deletePost,addComment

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add/$', addPost, name='add'),
    url(r'^delete/$',deletePost, name='delete'),
    url(r'^comment/$',addComment, name='comment'),
]