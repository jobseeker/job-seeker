from django import forms

class Postingan(forms.Form):
    attrs = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Say something!',
        'rows':'3',
        'cols':'3',
    }
    content = forms.CharField(widget=forms.Textarea(attrs=attrs))
    