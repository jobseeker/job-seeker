from django.shortcuts import render, redirect
from .models import Forum, Comment
from .forms import Postingan

# Views boi

response = {}
def index(request):
    forum = Forum.objects.all()
    response['forum'] = forum
    comment = Comment.objects.all()
    response['comment'] = comment
    response['Postingan'] = Postingan
    html = 'tugas2_forum.html'
    return render(request, html ,response)

def addPost(request):
    if request.method == 'POST':
        form = Postingan(request.POST or None)
        response['form'] = form
        if form.is_valid():
            response['content'] = request.POST['content']
            Forum.objects.create(body=form.cleaned_data['content'])
            return redirect ('/tugas2_forum/')
        else:
            return redirect ('/tugas2_forum/')

def deletePost(request):
    try:
        idStatus = request.POST['flag']
        Forum.objects.filter(id=idStatus).delete()
    except ValueError or KeyError:
        pass
    return redirect ('/tugas2_forum/')

def addComment(request):
    try:
        
        idStatus = request.POST['flag']
        target = Forum.objects.filter(id=idStatus)
    except ValueError or KeyError:
        pass
    form = Postingan(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['content'] = request.POST['content']
        Comment.objects.create(post_id=idStatus,body=form.cleaned_data['content'])
        return redirect ('/tugas2_forum/')
    else:
        return redirect ('/tugas2_forum/')

