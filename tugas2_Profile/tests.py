from django.test import TestCase, Client
from django.urls import resolve
from .views import index


class tugas2_Profile_UnitTest(TestCase):
	def test_tugas2_Profile_url_is_exist(self):
		response = Client().get('/tugas2_Profile/')
		self.assertEqual(response.status_code, 200)

	def test_tugas2_using_index_func(self):
		found = resolve('/tugas2_Profile/')
		self.assertEqual(found.func, index)

	def test_tugas2_template_using_tugas2_html(self):
		response = Client().get('/tugas2_Profile/')
		self.assertTemplateUsed(response, "tugas2_Profile/tugas2_Profile.html")



