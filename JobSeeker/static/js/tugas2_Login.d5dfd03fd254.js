// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", shareContent);
  }

// Handle the successful return from the API call
function onSuccess(data) {
  console.log(data);
}

// Handle an error response from the API call
function onError(error) {
  console.log(error);
}

// Use the API call wrapper to share content on LinkedIn
function shareContent() {
      
  // Build the JSON payload containing the content to be shared

    console.log("TEROTORISASI" + IN.User.isAuthorized());
    console.log("SUKSES!");
    
    IN.API.Raw("/companies?format=json&is-company-admin=true").result(
    function (data_user) {
        console.log(data_user);
        comp_data = data_user.values[0];
        comp_id = comp_data.id;
        IN.API.Raw("/companies/" + comp_id + ":(id,name,ticker,description,email-domains,website-url,founded-year,company-type,specialties)?format=json").result(
        saveSession);
    }
    ).error(failGetData);
    
}

function saveSession(data){
    sessionStorage.setItem('company', JSON.stringify(data));
    console.log(JSON.parse(sessionStorage.getItem('company')));
}

function failGetData(){
    console.log("error");
}