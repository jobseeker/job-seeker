"""JobSeeker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import url, include

import tugas2_Login.urls as tugas2_Login
import tugas2_Profile.urls as tugas2_Profile
import tugas2_forum.urls as tugas2_forum


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tugas2_Login/', include(tugas2_Login,namespace='tugas2_Login')),
    url(r'^tugas2_forum/', include(tugas2_forum,namespace='tugas2_forum')),
    url(r'^tugas2_Profile/', include(tugas2_Profile,namespace='tugas2_Profile')),

]